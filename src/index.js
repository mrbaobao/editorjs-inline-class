
/**
 * 
 * Enter key code
 */
const ENTER_KEY = 13

class InlineAddClass {
    static get isInline() {
        return true
    };
    static get isInline() {
        return true;
    }
    constructor({ api }) {
        //Selected text
        this.inputOpened = false
        /**
           * Elements
           */
        this.nodes = {
            button: null,
            input: null,
        }
        this.CSS = {
            button: 'ce-inline-tool',
            buttonActive: 'ce-inline-tool--active',
            buttonModifier: 'ce-inline-tool--link',
            buttonUnlink: 'ce-inline-tool--unlink',
            input: 'ce-inline-tool-input',
            inputShowed: 'ce-inline-tool-input--showed',
        }
        this.tag = "span"
        this.class = "cdx-marker"
        this.mark = null

        this.api = api;
        this.toolbar = api.toolbar;
        this.inlineToolbar = api.inlineToolbar;
        this.notifier = api.notifier;
        this.state = false
    }
    render() {
        this.nodes.button = document.createElement('button')
        this.nodes.button.type = "button"
        this.nodes.button.classList = "ce-inline-tool"
        this.nodes.button.innerHTML = `
           <svg xmlns="http://www.w3.org/2000/svg" 
                x="0px" y="0px"
                width="24" height="24"
                viewBox="0 0 172 172"
                style=" fill:#000000;">
                <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#666666"><path d="M37.625,21.5c-8.84221,0 -16.125,7.28279 -16.125,16.125v48.375h10.75v-48.375c0,-3.02579 2.34921,-5.375 5.375,-5.375h96.75c3.02579,0 5.375,2.34921 5.375,5.375v48.375h10.75v-48.375c0,-8.84221 -7.28279,-16.125 -16.125,-16.125zM37.625,96.75c-8.84221,0 -16.125,7.28279 -16.125,16.125v10.75v10.75c0,8.84221 7.28279,16.125 16.125,16.125c8.84221,0 16.125,-7.28279 16.125,-16.125h-10.75c0,3.02579 -2.34921,5.375 -5.375,5.375c-3.02579,0 -5.375,-2.34921 -5.375,-5.375v-10.75v-10.75c0,-3.02579 2.34921,-5.375 5.375,-5.375c3.02579,0 5.375,2.34921 5.375,5.375h10.75c0,-8.84221 -7.28279,-16.125 -16.125,-16.125zM80.625,96.75c-6.3425,0 -10.05226,2.56337 -12.05176,4.72412c-4.41825,4.76225 -4.0845,11.09988 -4.05225,11.40088c0,8.686 7.89571,12.60387 13.66846,15.47412c4.5795,2.26825 7.81055,4.02436 7.81055,6.10986c0,0.02151 -0.10347,2.62251 -1.51172,4.05226c-0.31175,0.31175 -1.20803,1.23877 -3.86328,1.23877h-15.13818c0.645,2.05325 1.67582,4.33108 3.63232,6.31983c1.98875,2.021 5.60411,4.43017 11.50586,4.43017c5.90175,0 9.52736,-2.41943 11.52686,-4.45117c4.5795,-4.65475 4.5879,-11.05033 4.57715,-11.67383c0,-8.815 -7.95769,-12.77133 -13.77344,-15.66308c-4.50425,-2.236 -7.67382,-3.9691 -7.68457,-6.17285c0,-0.0215 -0.09322,-2.43555 1.19678,-3.77929c0.7955,-0.82775 2.20073,-1.25977 4.15723,-1.25977h15.20117c-1.806,-5.40725 -6.42917,-10.75 -15.20117,-10.75zM123.625,96.75c-6.3425,0 -10.07326,2.56337 -12.07276,4.72412c-4.41825,4.76225 -4.0635,11.09988 -4.03125,11.40088c0,8.686 7.87471,12.60387 13.64746,15.47412c4.5795,2.26825 7.81055,4.02436 7.81055,6.10986c0,0.02151 -0.08247,2.62251 -1.49072,4.05226c-0.301,0.31175 -1.20803,1.23877 -3.86328,1.23877h-15.13818c0.645,2.05325 1.67582,4.33108 3.63233,6.31983c1.98875,2.021 5.6041,4.43017 11.50585,4.43017c5.90175,0 9.52736,-2.41943 11.52686,-4.45117c4.5795,-4.65475 4.5879,-11.05033 4.57715,-11.67383c0,-8.815 -7.95769,-12.77133 -13.77344,-15.66308c-4.50425,-2.236 -7.67382,-3.9691 -7.68457,-6.17285c0,-0.0215 -0.09322,-2.43555 1.19678,-3.77929c0.7955,-0.82775 2.20073,-1.25977 4.15723,-1.25977h15.20117c-1.80599,-5.3965 -6.42917,-10.75 -15.20117,-10.75z"></path></g></g>
            </svg>
        `
        return this.nodes.button
    }
    /**
       * Input for the link
       */
    renderActions() {
        this.nodes.input = document.createElement('input');
        this.nodes.input.placeholder = 'Type class names';
        this.nodes.input.classList.add(this.CSS.input);
        this.nodes.input.addEventListener('keydown', (event) => {
            if (event.keyCode === ENTER_KEY) {
                this.enterPressed(event);
            }
        });
        return this.nodes.input;
    }
    
    surround(range) {
        if (this.state) {
            return
        }
        const selectedText = range.extractContents();
        const mark = document.createElement(this.tag);

        mark.classList.add(this.class);
        mark.appendChild(selectedText);
        range.insertNode(mark);

        this.api.selection.expandToTag(mark);
        this.mark = mark
        /** Create blue background instead of selection */
        this.toggleActions()
    }

    toggleActions() {
        if (!this.inputOpened) {
            this.openActions(true);
        } else {
            this.closeActions(false);
        }
    }
    openActions(needFocus = false) {
        this.nodes.input.classList.add(this.CSS.inputShowed)
        if (needFocus) {
            this.nodes.input.focus()
        }
        this.inputOpened = true
        this.nodes.input.classList.add(this.CSS.inputShowed);
    }
    closeActions(clearSavedSelection = true) {
        if (clearSavedSelection) {
            this.nodes.input.value = ""
        }
        this.inputOpened = false
        /**
         * Close inline toolbar
         */
        this.inlineToolbar.close();
        this.nodes.input.classList.remove(this.CSS.inputShowed);
    }
    enterPressed(event) {
        var classNames = this.nodes.input.value || ""
        this.mark.classList.add(classNames)
        /**
         * Close actions
         */
        this.closeActions()
    }
    colorText(color) {
        document.execCommand("foreColor", false, color)
    }
    checkState(seleection) {
        const mark = this.api.selection.findParentTag(this.tag);

        this.state = !!mark;
    }
    /**
   * Sanitizer rule
   * @return {{span: {class: string}}}
   */
    static get sanitize() {
        return {
            span: {
                class: true // All classes accepted!
            }
        };
    }
}
//Export
export default InlineAddClass